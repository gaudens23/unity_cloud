﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Windows.Speech;



public class OBJSphere : MonoBehaviour
{
    private Dictionary<string, Action> keywordActions = new Dictionary<string, Action>();
    private KeywordRecognizer KeywordRecognizer;

    // Start is called before the first frame update
    void Start()
    {
       
        keywordActions.Add("Sphere Rouge",Sph);
        KeywordRecognizer = new KeywordRecognizer(keywordActions.Keys.ToArray());
        KeywordRecognizer.OnPhraseRecognized += OnKeywordsRecognized;
        KeywordRecognizer.Start();
    }
    private void OnKeywordsRecognized(PhraseRecognizedEventArgs args)
    {
        Debug.Log("Keyword:" + args.text);
        keywordActions[args.text].Invoke();

    }
    private void Sph()
    {
        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphere.transform.position = new Vector3(0f, 0f, 0f);
        var SphereRenderer = sphere.GetComponent<Renderer>();
        SphereRenderer.material.SetColor("_Color", Color.red);




    }

    // Update is called once per frame
    void Update()
    {
        
        

    }
}
 