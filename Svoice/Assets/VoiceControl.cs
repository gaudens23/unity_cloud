﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
public class VoiceControl : MonoBehaviour
{
    KeywordRecognizer KeywordRecognizerObj;
    public string[] Keywords_array;
    // Start is called before the first frame update
    void Start()
    {
        KeywordRecognizerObj = new KeywordRecognizer(Keywords_array);
        KeywordRecognizerObj.OnPhraseRecognized += OnKeywordsRecognized ;
        KeywordRecognizerObj.Start();
    }
    void OnKeywordsRecognized(PhraseRecognizedEventArgs args)
    {
        Debug.Log("Keyword" + args.text + "; Confidence " + args.confidence);
        ActionPerformer(args.text);
    }
    void ActionPerformer(string command)
    {
        if(command.Contains("Sphere"))
        {
            GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere.transform.position = new Vector3(0f, 2f, 2f);
        }

    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
