﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Windows.Speech;



public class OBJPlane: MonoBehaviour
{
    private Dictionary<string, Action> keywordActions = new Dictionary<string, Action>();
    private KeywordRecognizer KeywordRecognizer;
    // Start is called before the first frame update
    void Start()
    {

        keywordActions.Add("plane noir", plane);
        KeywordRecognizer = new KeywordRecognizer(keywordActions.Keys.ToArray());
        KeywordRecognizer.OnPhraseRecognized += OnKeywordsRecognized;
        KeywordRecognizer.Start();
    }
    private void OnKeywordsRecognized(PhraseRecognizedEventArgs args)
    {
        Debug.Log("Keyword:" + args.text);
        keywordActions[args.text].Invoke();

    }
    private void plane()
    {
        GameObject plane = GameObject.CreatePrimitive(PrimitiveType.Plane);
        var PlaneRenderer = plane.GetComponent<Renderer>();
        PlaneRenderer.material.SetColor("_Color", Color.black);




    }

    // Update is called once per frame
    void Update()
    {



    }
}
