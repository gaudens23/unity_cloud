﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Windows.Speech;



public class OBJCapsule : MonoBehaviour
{
    private Dictionary<string, Action> keywordActions = new Dictionary<string, Action>();
    private KeywordRecognizer KeywordRecognizer;
    // Start is called before the first frame update
    void Start()
    {

        keywordActions.Add("capsule vert", capsule);
        KeywordRecognizer = new KeywordRecognizer(keywordActions.Keys.ToArray());
        KeywordRecognizer.OnPhraseRecognized += OnKeywordsRecognized;
        KeywordRecognizer.Start();
    }
    private void OnKeywordsRecognized(PhraseRecognizedEventArgs args)
    {
        Debug.Log("Keyword:" + args.text);
        keywordActions[args.text].Invoke();

    }
    private void capsule()
    {
        GameObject capsule = GameObject.CreatePrimitive(PrimitiveType.Capsule);
        capsule.transform.position = new Vector3(2, 1, 0);
        var CapsuleRenderer = capsule.GetComponent<Renderer>();
        CapsuleRenderer.material.SetColor("_Color", Color.green);




    }

    // Update is called once per frame
    void Update()
    {



    }
}
