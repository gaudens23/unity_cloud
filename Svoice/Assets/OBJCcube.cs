﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Windows.Speech;



public class OBJCcube : MonoBehaviour
{
    private Dictionary<string, Action> keywordActions = new Dictionary<string, Action>();
    private KeywordRecognizer KeywordRecognizer;
    // Start is called before the first frame update
    void Start()
    {

        keywordActions.Add("cube bleu", cube);
        KeywordRecognizer = new KeywordRecognizer(keywordActions.Keys.ToArray());
        KeywordRecognizer.OnPhraseRecognized += OnKeywordsRecognized;
        KeywordRecognizer.Start();
    }
    private void OnKeywordsRecognized(PhraseRecognizedEventArgs args)
    {
        Debug.Log("Keyword:" + args.text);
        keywordActions[args.text].Invoke();

    }
    private void cube()
    {
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.transform.position = new Vector3(0f, 3f, 3f);
        var CubeRenderer = cube.GetComponent<Renderer>();
        CubeRenderer.material.SetColor("_Color", Color.blue);




    }

    // Update is called once per frame
    void Update()
    {



    }
}
